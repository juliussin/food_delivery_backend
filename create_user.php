<?php
    /* 
    Post:   'email'
            'password'
            'first_name'
            'last_name'
            'address'
            'balance'
    
    Return: 'status'        0 (failed) or 1 (success)     
            'message'       Status Message
    */
    
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin, Content-Type, 
        Access-Control-Allow-Methods, Access-Control-Allow-Headers, Authorization, X-Requested-With');

    include_once 'models/User.php';
    include_once 'models/Database.php';
    
    $database = new Database();
    $db = $database->connect();
    
    $user = new User($db);
    
    // $data = json_decode(file_get_contents("php://input"));

    $user->email = isset($_GET['email']) ? $_GET['email'] : die();
    $user->password = isset($_GET['password']) ? $_GET['password'] : die();
    $user->first_name = isset($_GET['first_name']) ? $_GET['first_name'] : die();
    $user->last_name = isset($_GET['last_name']) ? $_GET['last_name'] : die();
    $user->address = isset($_GET['address']) ? $_GET['address'] : die();
    $user->balance = isset($_GET['balance']) ? $_GET['balance'] : die();
    
    if ($user->create()) {
        $arr = array(
            'status' => 1,
            'message' => 'create user success'
        );
    } else {
        $arr = array(
            'status' => 0,
            'message' => 'create user failed'
        );
    };

    echo json_encode($arr);
?>