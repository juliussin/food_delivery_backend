<?php
    /* 
    Post:   'param'         'order_id' or 'merchant_id' or 'rider_id' or 'user_id' or 'status' or else
            'order_id'      Query by Menu ID
            'merchant_id'   Query by Merchant ID
            'rider_id'      Query by Rider ID
            'user_id'       Query by User ID
            'status'        Query by Status
    
    Return: 'status'        0 (failed) or 1 (success)     
            'data'          '' (failed)
                            [order1, order2, ...] (success)
                            order: '', ''
    */

    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin, Content-Type, 
        Access-Control-Allow-Methods, Access-Control-Allow-Headers, Authorization, X-Requested-With');

    include_once 'models/Menu.php';
    include_once 'models/Database.php';
    
    $database = new Database();
    $db = $database->connect();
    
    $order = new Order($db);
    
    $param = isset($_POST['param']) ? $_POST['param'] : '';
    $order->order_id = isset($_POST['order_id']) ? $_POST['order_id'] : '';
    $order->merchant_id = isset($_POST['merchant_id']) ? $_POST['merchant_id'] : '';
    $order->rider_id = isset($_POST['rider_id']) ? $_POST['rider_id'] : '';
    $order->user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
    $order->status = isset($_POST['status']) ? $_POST['status'] : '';

    $result = $order->read($param);
    if ($result->rowCount() == 0) {
        $arr = array(
            'status' => 0,
            'data' => ''
        );
        echo json_encode($arr);
    } else {
        $arr = array();
        $arr['status'] = 1;
        $arr['data'] = array();
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            array_push($arr['data'], $row);
        }
        echo json_encode($arr);
    };

?>