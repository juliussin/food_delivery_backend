<?php
    /* 
    Post:   'time'
            'status'        default 0
            'fee'
            'total_price'
            'merchant_id'
            'rider_id'      (optional)
            'user_id'
    
    Return: 'status'        0 (failed) or 1 (success)     
            'message'       Status Message
    */

    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin, Content-Type, 
        Access-Control-Allow-Methods, Access-Control-Allow-Headers, Authorization, X-Requested-With');

    include_once 'models/Order.php';
    include_once 'models/Database.php';
    
    $database = new Database();
    $db = $database->connect();
    
    $order = new Order($db);
    
    $order->time = isset($_POST['time']) ? $_POST['time'] : die();
    $order->status = isset($_POST['status']) ? $_POST['status'] : 0;
    $order->fee = isset($_POST['fee']) ? $_POST['fee'] : die();
    $order->total_price = isset($_POST['total_price']) ? $_POST['total_price'] : die();
    $order->merchant_id = isset($_POST['merchant_id']) ? $_POST['merchant_id'] : die();
    $order->rider_id = isset($_POST['rider_id']) ? $_POST['rider_id'] : '';
    $order->user_id = isset($_POST['user_id']) ? $_POST['user_id'] : die();

    if ($order->create()) {
        $arr = array(
            'status' => 1,
            'message' => 'create order success'
        );
    } else {
        $arr = array(
            'status' => 0,
            'message' => 'create order failed'
        );
    }

    echo json_encode($arr);
?>