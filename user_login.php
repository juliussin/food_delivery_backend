<?php
    /* 
    Post:   'email'
            'password'
    
    Return: 'status'        0 (failed) or 1 (success)     
            'data'          '' (failed)
                            user (success)
                            user: 'user_id', 'email', 'first_name', 'last_name', 'address', 'balance'
    */

    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin, Content-Type, 
        Access-Control-Allow-Methods, Access-Control-Allow-Headers, Authorization, X-Requested-With');

    include_once 'models/User.php';
    include_once 'models/Database.php';
    
    $database = new Database();
    $db = $database->connect();
    
    $user = new User($db);
    
    $user->email = isset($_POST['email']) ? $_POST['email'] : die();
    $user->password = isset($_POST['password']) ? $_POST['password'] : die();

    $result = $user->login();
    if ($result->rowCount() == 0) {
        $arr = array(
            'status' => 0,
            'data' => ''
        );
        echo json_encode($arr);
    } else {
        $arr = array();
        $arr['status'] = 1;

        $row = $result->fetch(PDO::FETCH_ASSOC);

        $arr['data'] = $row;
        echo json_encode($arr);
    };
?>