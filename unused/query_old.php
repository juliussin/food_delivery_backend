<?php
    include "connection.php";
    $sql = $_GET["query"];
    $result = $conn->query($sql);
    $response = [];
    while($row = $result->fetch_assoc()) {
        $response[] = $row;
    }
    echo json_encode($response);
?>
