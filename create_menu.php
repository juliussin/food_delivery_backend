<?php
    /* 
    Post:   'name'          Menu Name
            'image'         Menu Image in Binary
            'price'         Menu Price
            'merchant_id'   Merchant ID
            'categories'    Menu Categories (multiple values ​​separated by semicolons)
    
    Return: 'status'        0 (failed) or 1 (success)     
            'message'       Status Message
    */

    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin, Content-Type, 
        Access-Control-Allow-Methods, Access-Control-Allow-Headers, Authorization, X-Requested-With');

    include_once 'models/Menu.php';
    include_once 'models/Database.php';
    
    $database = new Database();
    $db = $database->connect();
    
    $menu = new Menu($db);
    
    // $data = json_decode(file_get_contents("php://input"));
    
    $menu->name = isset($_POST['name']) ? $_POST['name'] : die();
    $menu->image = '';
    // $menu->image = isset($_GET['image']) ? 
    //     file_get_contents($_FILES['image']['tmp_name']) : 
    //     '';
    $menu->price = isset($_POST['price']) ? $_POST['price'] : die();
    $menu->merchant_id = isset($_POST['merchant_id']) ? $_POST['merchant_id'] : die();
    $menu->categories = isset($_POST['categories']) ? $_POST['categories'] : '';
    $menu->categories = explode(';', $menu->categories);
    if ($menu->create()) {
        $arr = array(
            'status' => 1,
            'message' => 'create user success'
        );
    } else {
        $arr = array(
            'status' => 0,
            'message' => 'create user failed'
        );
    }

    echo json_encode($arr);
?>