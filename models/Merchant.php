<?php
    class Merchant {
        private $conn;
        private $table = 'merchants';
        
        public $merchant_id;
        public $email;
        public $password;
        public $name;
        public $address;
        public $balance;

        public function __construct($db) {
            $this->conn = $db;
        }

        public function read() {
            $query = "SELECT 
                FROM".$this->table."
                
            ";
        }

        public function create() {
            $query = 'INSERT INTO '.$this->table.
                ' SET email = ?,
                password = ?,
                name = ?,
                address = ?,
                balance = ?';

            $statement = $this->conn->prepare($query);
            
            $statement->bindParam(1, $this->email, PDO::PARAM_STR);
            $statement->bindParam(2, $this->password, PDO::PARAM_STR);
            $statement->bindParam(3, $this->name, PDO::PARAM_STR);
            $statement->bindParam(4, $this->address, PDO::PARAM_STR);
            $statement->bindParam(5, $this->balance, PDO::PARAM_STR);

            if ($statement->execute()) {
                return true;
            } else {
                printf('error: %s\n', $statement->error);
                return false;
            }
        }

        public function login() {
            $query = 'SELECT merchant_id, name, address, balance FROM '.
                $this->table.' WHERE email = ? AND password = ?';

            $statement = $this->conn->prepare($query);
            
            $statement->bindParam(1, $this->email, PDO::PARAM_STR);
            $statement->bindParam(2, $this->password, PDO::PARAM_STR);
            try {
                $statement->execute();
            } catch (PDOException $e) {
                echo "error: ".$e->getMessage();
            }

            return $statement;
        }

        public function search($key) {
            $query = 'SELECT * FROM '.$this->table.' WHERE name = %?%';
            $statement = $this->conn->prepare($query);
            $statement->bindParam(1, $key);
            try {
                $statement->execute();
            } catch (PDOException $e) {
                echo "error: ".$e->getMessage();
            }
            return $statement;
        }
    }

?>