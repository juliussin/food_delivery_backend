<?php 
    class Database {
        private $host = "localhost";
        private $db_name = "food_delivery_db";
        private $username = "root";
        private $password = "password";
        private $conn;

        public function connect(){
            $this->conn = null;
            try {
                $this->conn = new PDO("mysql:host=".$this->host.";dbname=".
                              $this->db_name, $this->username, $this->password);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);
                // $this->conn->exec("set names utf8");
                // echo "connection success";
            } catch(PDOException $e) {
                echo "connection error: ".$e->getMessage();
            }
            return $this->conn;
        }
    }  
?>
