<?php
    class Rider {
        private $conn;
        private $table = 'riders';
        
        public $rider_id;
        public $email;
        public $password;
        public $first_name;
        public $last_name;
        public $address;
        public $balance;

        public function __construct($db) {
            $this->conn = $db;
        }

        public function read() {
            $query = "SELECT 
                FROM".$this->table."
                
            ";
        }

        public function create() {
            $query = 'INSERT INTO '.$this->table.
                ' SET email = ?,
                password = ?,
                first_name = ?,
                last_name = ?,
                address = ?,
                balance = ?';

            $statement = $this->conn->prepare($query);
            
            $statement->bindParam(1, $this->email, PDO::PARAM_STR);
            $statement->bindParam(2, $this->password, PDO::PARAM_STR);
            $statement->bindParam(3, $this->first_name, PDO::PARAM_STR);
            $statement->bindParam(4, $this->last_name, PDO::PARAM_STR);
            $statement->bindParam(5, $this->address, PDO::PARAM_STR);
            $statement->bindParam(6, $this->balance, PDO::PARAM_STR);

            if ($statement->execute()) {
                return true;
            } else {
                printf('error: %s\n', $statement->error);
                return false;
            }
        }

        public function login() {
            $query = 'SELECT rider_id, first_name, last_name, address, balance FROM '.
                $this->table.' WHERE email = ? AND password = ?';

            $statement = $this->conn->prepare($query);
            
            $statement->bindParam(1, $this->email, PDO::PARAM_STR);
            $statement->bindParam(2, $this->password, PDO::PARAM_STR);
            try {
                $statement->execute();
            } catch (PDOException $e) {
                echo "error: ".$e->getMessage();
            }

            return $statement;
        }
    }

?>