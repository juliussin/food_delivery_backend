<?php
    class Menu {
        private $conn;
        private $table = 'menus';
        private $cat_table = 'categories';
        private $cat_string;
        
        public $menu_id;
        public $name;
        // public $image;
        public $price;
        public $merchant_id;
        public $categories;
        public $category;

        public function __construct($db) {
            $this->conn = $db;
        }

        public function read($param) {
            // param: 'menu_id' to query by menu_id
            //        'merchant_id' to query by merchant_id
            //        'category' to query by category
            //        else will query all menu available.

            if (strcasecmp($param, 'menu_id') == 0) {
                // echo 'menu_id';
                $query = 'SELECT menu_id, name, image, price, merchant_id FROM '.
                    $this->table.' WHERE menu_id = ?';
                $statement = $this->conn->prepare($query);
                $statement->bindParam(1, $this->menu_id);
            } elseif (strcasecmp($param, 'merchant_id') == 0) {
                // echo 'merchant_id';
                $query = 'SELECT menu_id, name, image, price, merchant_id FROM '.
                    $this->table.' WHERE merchant_id = ?';
                $statement = $this->conn->prepare($query);
                $statement->bindParam(1, $this->merchant_id);
            } elseif (strcasecmp($param, 'category') == 0) {
                // echo 'category';
                $result = $this->findby_category();
                $ids = [];
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    $ids[] = $row['menu_id'];
                }
                $query = 'SELECT menu_id, name, image, price, merchant_id FROM '.
                    $this->table.' WHERE merchant_id IN ('.
                    implode(',', array_map('intval', $ids)).')';
                $statement = $this->conn->prepare($query);
            } else {
                // echo 'all';
                $query = 'SELECT menu_id, name, image, price, merchant_id FROM '.
                    $this->table;
                $statement = $this->conn->prepare($query);
            }
            try {
                $statement->execute();
            } catch (PDOException $e) {
                echo "error: ".$e->getMessage();
            }
            return $statement;
        }
        
        public function findby_category() {
            $query = 'SELECT menu_id FROM '.$this->cat_table.
                ' WHERE category = ?';
            $statement = $this->conn->prepare($query);
            $statement->bindParam(1, $this->category);
            try {
                $statement->execute();
            } catch (PDOException $e) {
                echo "error: ".$e->getMessage();
            }
            return $statement;
        }
        
        function cat_string_constructor() {
            $str = '';
            for ($x = 0; $x < count($this->categories); $x++) {
                $str = $str."('".$this->menu_id."', '".$this->categories[$x]."'),";
            };
            return substr($str, 0, -1);
        }

        public function create() {
            
            $query1 = 'INSERT INTO '.$this->table.
                ' SET name = ?,
                image = ?,
                price = ?,
                merchant_id = ?';
            
            $statement1 = $this->conn->prepare($query1);
            // $statement2 = $this->conn->prepare($query2);
            // $arr_cat = explode(';', $this->categories);
            // echo $this->categories[1];
            $statement1->bindParam(1, $this->name);
            $statement1->bindParam(2, $this->image);
            $statement1->bindParam(3, $this->price);
            $statement1->bindParam(4, $this->merchant_id);

            if ($statement1->execute()) {
                $this->menu_id = $this->conn->lastInsertId();
                $this->cat_string = $this->cat_string_constructor();
                $query2 = 'INSERT INTO '.$this->cat_table.
                    ' (menu_id, category) VALUES '.
                    $this->cat_string.';';              
                $statement2 = $this->conn->prepare($query2);
                if ($statement2->execute()) {
                    return true;
                } else {
                    
                    // printf('error2: %s\n', $statement2->error);
                    echo 'error2: '.$statement2->error;
                    return false;
                };
            } else {
                // printf('error1: %s\n', $statement1->error);
                echo 'error1: '.$statement1->error;
                return false;
            };
        }

        public function remove() {
            $query = 'DELETE FROM '.$this->table.
                ' WHERE menu_id = ?';
            $statement = $this->conn->prepare($query);
            $statement->bindParam(1, $this->menu_id);
            if ($statement->execute()) {
                return true;
            } else {                
                // printf('error: %s\n', $statement->error);
                echo 'error: '.$statement->error;
                return false;
            };
        }

        public function update() {
            $query1 = 'UPDATE '.$this->table.
                ' SET name = ?,
                image = ?,
                price = ?,
                merchant_id = ? WHERE menu_id = ?';
            
            $statement1 = $this->conn->prepare($query1);

            $statement1->bindParam(1, $this->name);
            $statement1->bindParam(2, $this->image);
            $statement1->bindParam(3, $this->price);
            $statement1->bindParam(4, $this->merchant_id);
            $statement1->bindParam(5, $this->menu_id);

            if ($statement1->execute()) {
                $this->cat_string = $this->cat_string_constructor();
                $query2 = 'DELETE FROM '.$this->cat_table.
                    ' WHERE menu_id = ?';
                $statement2 = $this->conn->prepare($query2);
                $statement2->bindParam(1, $this->menu_id);

                if ($statement2->execute()) {
                    $query3 = 'INSERT INTO '.$this->cat_table.
                        ' (menu_id, category) VALUES '.
                        $this->cat_string.';';              
                    $statement3 = $this->conn->prepare($query3);
                    if ($statement3->execute()) {
                        return true;
                    } else {
                        
                        // printf('error3: %s\n', $statement3->error);
                        echo 'error3: '.$statement3->error;
                        return false;
                    };
                } else {
                    
                    // printf('error2: %s\n', $statement2->error);
                    echo 'error2: '.$statement2->error;
                    return false;
                };
            } else {
                // printf('error1: %s\n', $statement1->error);
                echo 'error1: '.$statement1->error;
                return false;
            }
        }
    }

?>