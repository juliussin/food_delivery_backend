<?php
    class Order {
        private $conn;
        private $table = 'orders';
        private $detail_table = 'order_details';
        private $details_string;

        public $order_id;
        public $time;
        public $status;
        public $fee;
        public $total_price;
        public $merchant_id;
        public $rider_id;
        public $user_id;
        public $details;

        public function __construct($db) {
            $this->conn = $db;
        }

        public function create() {
            $query = 'INSERT INTO '.$this->table.
                ' SET time = ?, 
                status = ?, 
                fee = ?, 
                total_price = ?,
                merchant_id = ?,
                rider_id = ?,
                user_id = ?';
            $statement = $this->conn->prepare($query);
            $statement->bindParam(1, $this->time);
            $statement->bindParam(2, $this->status);
            $statement->bindParam(3, $this->fee);
            $statement->bindParam(4, $this->total_price);
            $statement->bindParam(5, $this->merchant_id);
            $statement->bindParam(6, $this->rider_id);
            $statement->bindParam(7, $this->user_id);

            if ($statement->execute()) {
                return true;
            } else {
                return false;
            }
        }

        function details_string_constructor() {
            $str = '';
            for ($x = 0; $x < count($this->details); $x++) {
                $str = $str."('".$this->order_id."', '".
                    $this->details[$x]['menu_id']."', '".
                    $this->details[$x]['price']."', '".
                    $this->details[$x]['qty']."),";
            };
            return substr($str, 0, -1);
        }

        public function create_details() {
            $this->order_id = $this->conn->lastInsertId();
            $this->details_string = $this->details_string_constructor();
            $query = 'INSERT INTO '.$this->detail_table.
                ' (order_id, menu_id, price, qty) VALUES '.
                $this->details_string.';';
            $statement = $this->conn->prepare($query);

            if ($statement->execute()) {
                return true;
            } else {
                echo 'error: '.$statement->error;
                return false;
            }
        }

        public function remove_details($str = 'true') {
            $query = 'DELETE FROM '.$this->detail_table.
                ' WHERE order_id = ? AND ?';
            $statement = $this->conn->prepare($query);
            $statement->bindParam(1, $this->order_id);
            $statement->bindParam(2, $str);
            if ($statement->execute()) {
                return true;
            } else {                
                // printf('error: %s\n', $statement->error);
                echo 'error: '.$statement->error;
                return false;
            };
        }

        public function read_status() {
            $query = 'SELECT status FROM '.$this->table.
                ' WHERE order_id = ?';
            $statement = $this->conn->prepare($query);
            $statement->bindParam(1, $this->order_id);
            
            try {
                $statement->execute();
            } catch (PDOException $e) {
                echo "error: ".$e->getMessage();
            }

            if ($statement->rowCount() == 0) {
                $arr = [];
                $arr['status'] = 0;
                $arr['data'] = '';
            } else {
                $arr = [];
                $arr['status'] = 1;
                $row = $result->fetch(PDO::FETCH_ASSOC);
                $arr['data'] = $row['status'];
            };
            return $arr;
            // return $statement;
        }

        public function read($param) {
            // param: 'order_id'
            //        'merchant_id'
            //        'rider_id'
            //        'user_id'
            //        'status'
            //        else will query all the order available

            if (strcasecmp($param, 'order_id') == 0) {
                $query = 'SELECT * FROM '.$this->table.' WHERE order_id = ?';
                $statement = $this->conn->prepare($query);
                $statement->bindParam(1, $this->order_id);
            } elseif (strcasecmp($param, 'merchant_id') == 0) {
                $query = 'SELECT * FROM '.$this->table.' WHERE merchant_id = ?';
                $statement = $this->conn->prepare($query);
                $statement->bindParam(1, $this->merchant_id);
            } elseif (strcasecmp($param, 'rider_id') == 0) {
                $query = 'SELECT * FROM '.$this->table.' WHERE rider_id = ?';
                $statement = $this->conn->prepare($query);
                $statement->bindParam(1, $this->rider_id);
            } elseif (strcasecmp($param, 'user_id') == 0) {
                $query = 'SELECT * FROM '.$this->table.' WHERE user_id = ?';
                $statement = $this->conn->prepare($query);
                $statement->bindParam(1, $this->user_id);
            } elseif (strcasecmp($param, 'status') == 0) {
                $query = 'SELECT * FROM '.$this->table.' WHERE status = ?';
                $statement = $this->conn->prepare($query);
                $statement->bindParam(1, $this->status);
            } else {
                $query = 'SELECT * FROM '.$this->table;
                $statement = $this->conn->prepare($query);
            }

            try {
                $statement->execute();
            } catch (PDOException $e) {
                echo "error: ".$e->getMessage();
            }
            return $statement;
        }

        public function update($param) {
            // param: 'rider_id'
            //        'status'

            if (strcasecmp($param, 'rider_id') == 0) {
                $query = 'UPDATE '.$this->table.' SET rider_id = ? WHERE order_id = ?';
                $statement = $this->conn->prepare($query);
                $statement->bindParam(1, $this->rider_id);
                $statement->bindParam(2, $this->order_id);

            } elseif (strcasecmp($param, 'status') == 0) {
                $query = 'UPDATE '.$this->table.' SET status = ? WHERE order_id = ?';
                $statement = $this->conn->prepare($query);
                $statement->bindParam(1, $this->status);
                $statement->bindParam(2, $this->order_id);
            } else {
                echo 'false param';
                return false;
            }

            if ($statement->execute()) {
                return true;
            } else {
                // printf('error: %s\n', $statement->error);
                echo 'error: '.$statement->error;
                return false;
            }
        }
    }


?>