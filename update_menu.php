<?php
    /* 
    Post:   'menu_id'       NOT NULL
            'name'          NOT NULL
            'image'         
            'price'         NOT NULL
            'merchant_id'   NOT NULL
            'categories'
    
    Return: 'status'        0 (failed) or 1 (success)     
            'message'       Status Message
    */

    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin, Content-Type, 
        Access-Control-Allow-Methods, Access-Control-Allow-Headers, Authorization, X-Requested-With');

    include_once 'models/Menu.php';
    include_once 'models/Database.php';
    
    $database = new Database();
    $db = $database->connect();
    
    $menu = new Menu($db);
    
    // $data = json_decode(file_get_contents("php://input"));
    $menu->menu_id = isset($_POST['menu_id']) ? $_POST['menu_id'] : die();
    $menu->name = isset($_POST['name']) ? $_POST['name'] : die();
    $menu->image = isset($_POST['name']) ? $_POST['name'] : '';
    // $menu->image = isset($_GET['image']) ? 
    //     file_get_contents($_FILES['image']['tmp_name']) : 
    //     '';
    $menu->price = isset($_POST['price']) ? $_POST['price'] : die();
    $menu->merchant_id = isset($_POST['merchant_id']) ? $_POST['merchant_id'] : die();
    $menu->categories = isset($_POST['categories']) ? $_POST['categories'] : '';
    $menu->categories = explode(';', $menu->categories);
    if ($menu->update()) {
        $arr = array(
            'status' => 1,
            'message' => 'update menu success'
        );
    } else {
        $arr = array(
            'status' => 0,
            'message' => 'update menu failed'
        );
    }

    echo json_encode($arr);
?>