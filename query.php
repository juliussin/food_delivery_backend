<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once 'models/User.php';
    include_once 'models/Database.php';
    
    $database = new Database();
    $db = $database->connect();
    
    $query = $_GET['query'];

    $statement = $db->prepare($query);
    
    if ($statement->execute()) {
        $response = array();
        while($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            array_push($response, $row);
        }
        echo json_encode($response);
    } else {
        echo 'error: %s\n', $statement->error;
    }

?>