<?php
    /* 
    Post:   'menu_id'       Menu ID to be deleted
    
    Return: 'status'        0 (failed) or 1 (success)     
            'message'       Status Message
    */

    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin, Content-Type, 
        Access-Control-Allow-Methods, Access-Control-Allow-Headers, Authorization, X-Requested-With');

    include_once 'models/Menu.php';
    include_once 'models/Database.php';
    
    $database = new Database();
    $db = $database->connect();
    
    $menu = new Menu($db);
    
    // $data = json_decode(file_get_contents("php://input"));
    $menu->menu_id = isset($_POST['menu_id']) ? $_POST['menu_id'] : die();
    if ($menu->remove()) {
        $arr = array(
            'status' => 1,
            'message' => 'remove menu success'
        );
    } else {
        $arr = array(
            'status' => 0,
            'message' => 'remove menu failed'
        );
    };

    echo json_encode($arr);

?>