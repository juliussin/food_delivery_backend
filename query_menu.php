<?php
    /* 
    Post:   'param'         'menu_id' or 'merchant_id' or 'category' or else
            'menu_id'       Query by Menu ID
            'merchant_id'   Query by Merchant ID
            'category'      Query by Category
    
    Return: 'status'        0 (failed) or 1 (success)     
            'data'          '' (failed)
                            [menu1, menu2, ...] (success)
                            menu: 'menu_id', 'name', 'image', 'price', 'merchant_id'
    */

    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Origin, Content-Type, 
        Access-Control-Allow-Methods, Access-Control-Allow-Headers, Authorization, X-Requested-With');

    include_once 'models/Menu.php';
    include_once 'models/Database.php';
    
    $database = new Database();
    $db = $database->connect();
    
    $menu = new Menu($db);
    
    // $data = json_decode(file_get_contents("php://input"));
    $param = isset($_POST['param']) ? $_POST['param'] : '';
    $menu->menu_id = isset($_POST['menu_id']) ? $_POST['menu_id'] : '';
    $menu->merchant_id = isset($_POST['merchant_id']) ? $_POST['merchant_id'] : '';
    $menu->category = isset($_POST['category']) ? $_POST['category'] : '';

    $result = $menu->read($param);
    if ($result->rowCount() == 0) {
        $arr = array(
            'status' => 0,
            'data' => ''
        );
        echo json_encode($arr);
    } else {
        $arr = array();
        $arr['status'] = 1;
        $arr['data'] = array();
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            array_push($arr['data'], $row);
        }
        echo json_encode($arr);
    };

?>